<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>

<?php

class FunString{
    
    private $myString;
    private $myArray;
    
    public function __construct($x) {
        $this->myString =(string)$x;
        $this->action();
        } 
        
    private function action(){
        $this->myArray = explode(' ', $this->myString);
        foreach ($this->myArray as $key => $value) {
            if($value=='')
                unset ($this->myArray[$key]);
        }
        $this->myArray = array_values($this->myArray);
    }
        
    public function getFirstName(){
        return $this->myArray[1];
    }
        
    public function getLastName(){
        return $this->myArray[0];
    }
        
    public function getDate(){
        return date_create($this->myArray[count($this->myArray) - 1]);
    }
}

$foo = new FunString('    Куренков      Владимир    Вячеславович    12.6.94  ');
echo $foo->getLastName() . "<br>";
echo $foo->getFirstName() . "<br>";
echo date_format($foo->getDate(), "d.m.Y") . "<br>";

?>

    </body>
</html>